push:
	git pull
	git add .
	git commit -m "update" || true
	git push origin main || true

gitleaks:
	docker run -v  .:/path \
	 	zricethezav/gitleaks:v8.18.1  --source="/path" detect -v
